<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController; 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::redirect('/', '/users');

Route::prefix('users')->group(function () {
    Route::get('/', [UserController::class,'index'])->name('user.index');
    Route::get('/create', [UserController::class,'create'])->name('user.create');
    Route::post('/create', [UserController::class,'store'])->name('user.store');
    Route::get('/edit/{id}', [UserController::class,'edit'])->middleware('auth')->name('user.edit');
    Route::put('/update/{id}', [UserController::class,'update'])->middleware('auth')->name('user.update');
    Route::delete('/destroy/{id}', [UserController::class,'destroy'])->middleware('auth')->name('user.destroy');
    Route::get('/show/{id}', [UserController::class,'show'])->name('user.show');
});

