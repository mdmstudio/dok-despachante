<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController; 

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/api/user', function (Request $request) {
    return $request->user();
});

Route::prefix('users')->as('api.')->group(function () {
    Route::post('/create', [UserController::class,'store'])->name('user.store');
    Route::put('/update/{id}', [UserController::class,'update'])->name('user.update');
    Route::delete('/destroy/{id}', [UserController::class,'destroy'])->name('user.destroy');
});
