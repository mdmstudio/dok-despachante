<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Eloquent::unguard();

        if ($this->command->confirm('Deseja rodar uma "Fresh Migration" antes de executar o Seeder? Isto vai limpar todos os registros antigos')) {
            $this->command->call('migrate:fresh');
            $this->command->line("Banco de dados recriado");
        }

        $this->call(UserSeeder::class);
        $this->command->info("Banco de dados pronto");

       \Eloquent::reguard();

    }
}
