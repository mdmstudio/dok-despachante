<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $count = (int)$this->command->ask('Alimentar os usuários iniciais com quantos registros?', 250);
        $this->command->info("Criando {$count} usuário(s).");

        if(!$count){
            $this->command->info('Vamos adicionar pelo menos dois registros para execução de testes');
            $count = 2;
        }

        User::factory()->count($count)->create();
        $this->command->info('Registro(s) criado(s)');

        $this->command->info(PHP_EOL . '------ +');
        $this->command->info('Para rodar os testes de aplicação');
        $this->command->info('php artisan test');
        $this->command->info('------ +'.PHP_EOL);

    }
}
