@extends('layouts.app')

@section('content')

    <div class="mt-8 dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        <div class="grid grid-cols-1 md:grid-cols-1">
            
            @if (isset($data) && $data)

            <div class="p-6 border-t border-gray-200 dark:border-gray-700">
                <div class="flex items-center relative">
                    <img src="{{asset('svg/chat.svg')}}" alt="Usuários" width="30" height="auto">
                    <div class="ml-4 leading-7 dark:text-white" style="width:60%">
                        <h1 class="content-80">{{$data->name}}</h1>
                    </div>

                    <div>
                        <a href="{{ route('user.index') }}" class="btn btn-warning btn-new">Voltar</a>
                    </div>

                </div>
                
                <div class="ml-12 text-gray-600 dark:text-gray-400 text-sm content-80">
                    {!!$data->description!!}
                </div>

                <table class="table my-5 dark:text-white" style="font-size:0.7em">

                    <tr>
                        <th>E-mail</th>
                        <th>Tipo</th>
                        <th>Localidade</th>
                        <th>Status</th>
                        <th>Data</th>
                        <th>&nbsp;</th>
                    </tr>

                    <tr>
                        <td><a href="mailto:{{ $data->email }}" title="Enviar mensagem">{{ $data->email }}</a></td>
                        <td><span class="badge badge-{{ strtolower($data->type) }}">{{ $data->type() }}</span></td>
                        <td>{{ $data->uf }}</td>
                        <td><span class="badge badge-{{ ($data->status) ? 'active' : 'inactive' }}">{{ $data->status() }}</span></td>
                        <td>{{ $data->created_at() }}</td>
                        <td class="actions-icons">

                            @if (\Auth::user())

                            <a href="{{ route('user.edit',[$data->id]) }}" class="mx-2"><img src="{{asset('svg/edit.svg')}}" alt="Atualizar registro" width="15" height="auto" /></a>
                            <form action="{{ route('user.destroy',[$data->id]) }}" method="POST" enctype="application/x-www-form-urlencoded" class="d-inline-block">
                                @method('DELETE')
                                @csrf
                                <button type="submit"><img src="{{asset('svg/trash.svg')}}" alt="Remover registro" width="15" height="auto" /></button>
                            </form>

                            @endif

                        </td>
                    </tr>
                </table>
                
            </div>

        @else

        <div class="alert alert-danger text-center">Nenhum dado encontrado</div>
            
        @endif

        </div>
    </div>

@endsection