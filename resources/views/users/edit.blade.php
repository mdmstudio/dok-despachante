@extends('layouts.app')

@section('content')

    <div class="mt-8 dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        <div class="grid grid-cols-1 md:grid-cols-1">
            
            <div class="p-6 border-t border-gray-200 dark:border-gray-700">
                <div class="flex items-center relative">
                    <img src="{{asset('svg/chat.svg')}}" alt="Usuários" width="30" height="auto">
                    <div class="ml-4 leading-7 dark:text-white">
                        <h1>Atualizar usuário</h1>
                    </div>
                    <div>
                        <a href="{{ route('user.index') }}" class="btn btn-dark btn-new">Voltar</a>
                    </div>
                </div>

                <form name="form" id="form" method="POST" enctype="application/x-www-form-urlencoded" class="my-5">
                    @method('PUT')
                    @csrf
                    <div class="row mb-3">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="title">Nome</label>
                                <input type="text" class="form-control" name="name" placeholder="Nome do usuário" value="{{(isset($data->name) && $data->name ? $data->name : old('name'))}}" maxlength="60" required />
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="title">E-mail</label>
                                <input type="email" class="form-control" name="email" placeholder="E-mail/credencial" value="{{(isset($data->email) && $data->email ? $data->email : old('email'))}}" maxlength="155" required />
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="title">Senha</label>
                                <input type="password" class="form-control" name="password" placeholder="Senha" maxlength="20" />
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="title">Confirme a Senha</label>
                                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirmação de Senha" maxlength="20" />
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="description">Observações</label>
                                <textarea class="form-control" name="description" placeholder="Observações do usuário" rows="3" required>{{(isset($data->description) && $data->description ? $data->description : old('description'))}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-12 col-sm-5">
                            <div class="form-group">
                                <label for="type">Tipo de Cadastro</label>
                                <select class="form-control" name="type" required>
                                    <option value="PF" {{(isset($data->type) && strtoupper($data->type)==='PF' || old('type')==='PF' ? 'selected="selected"' : '')}}>Pessoa Física</option>
                                    <option value="PJ" {{(isset($data->type) && strtoupper($data->type)==='PJ' || old('type')==='PJ' ? 'selected="selected"' : '')}}>Pessoa Jurídica</option>
                                    <option value="BOTH" {{(isset($data->type) && strtoupper($data->type)==='BOTH' || old('type')==='BOTH' ? 'selected="selected"' : '')}}>Pessoa física e jurídica</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                <label for="uf">Localidade</label>
                                <select class="form-control" name="uf" required>
                                    <option {{ (isset($data->uf) && $data->uf==='AC' || old('uf')==='AC' ? 'selected="selected"' : '') }}>AC</option>
                                    <option {{ (isset($data->uf) && $data->uf==='AL' || old('uf')==='AL' ? 'selected="selected"' : '') }}>AL</option>
                                    <option {{ (isset($data->uf) && $data->uf==='AP' || old('uf')==='AP' ? 'selected="selected"' : '') }}>AP</option>
                                    <option {{ (isset($data->uf) && $data->uf==='AM' || old('uf')==='AM' ? 'selected="selected"' : '') }}>AM</option>
                                    <option {{ (isset($data->uf) && $data->uf==='BA' || old('uf')==='BA' ? 'selected="selected"' : '') }}>BA</option>
                                    <option {{ (isset($data->uf) && $data->uf==='CE' || old('uf')==='CE' ? 'selected="selected"' : '') }}>CE</option>
                                    <option {{ (isset($data->uf) && $data->uf==='DF' || old('uf')==='DF' ? 'selected="selected"' : '') }}>DF</option>
                                    <option {{ (isset($data->uf) && $data->uf==='ES' || old('uf')==='ES' ? 'selected="selected"' : '') }}>ES</option>
                                    <option {{ (isset($data->uf) && $data->uf==='GO' || old('uf')==='GO' ? 'selected="selected"' : '') }}>GO</option>
                                    <option {{ (isset($data->uf) && $data->uf==='MA' || old('uf')==='MA' ? 'selected="selected"' : '') }}>MA</option>
                                    <option {{ (isset($data->uf) && $data->uf==='MT' || old('uf')==='MT' ? 'selected="selected"' : '') }}>MT</option>
                                    <option {{ (isset($data->uf) && $data->uf==='MS' || old('uf')==='MS' ? 'selected="selected"' : '') }}>MS</option>
                                    <option {{ (isset($data->uf) && $data->uf==='MG' || old('uf')==='MG' ? 'selected="selected"' : '') }}>MG</option>
                                    <option {{ (isset($data->uf) && $data->uf==='PA' || old('uf')==='PA' ? 'selected="selected"' : '') }}>PA</option>
                                    <option {{ (isset($data->uf) && $data->uf==='PB' || old('uf')==='PB' ? 'selected="selected"' : '') }}>PB</option>
                                    <option {{ (isset($data->uf) && $data->uf==='PR' || old('uf')==='PR' ? 'selected="selected"' : '') }}>PR</option>
                                    <option {{ (isset($data->uf) && $data->uf==='PE' || old('uf')==='PE' ? 'selected="selected"' : '') }}>PE</option>
                                    <option {{ (isset($data->uf) && $data->uf==='PI' || old('uf')==='PI' ? 'selected="selected"' : '') }}>PI</option>
                                    <option {{ (isset($data->uf) && $data->uf==='RJ' || old('uf')==='RJ' ? 'selected="selected"' : '') }}>RJ</option>
                                    <option {{ (isset($data->uf) && $data->uf==='RN' || old('uf')==='RN' ? 'selected="selected"' : '') }}>RN</option>
                                    <option {{ (isset($data->uf) && $data->uf==='RS' || old('uf')==='RS' ? 'selected="selected"' : '') }}>RS</option>
                                    <option {{ (isset($data->uf) && $data->uf==='RO' || old('uf')==='RO' ? 'selected="selected"' : '') }}>RO</option>
                                    <option {{ (isset($data->uf) && $data->uf==='RR' || old('uf')==='RR' ? 'selected="selected"' : '') }}>RR</option>
                                    <option {{ (isset($data->uf) && $data->uf==='SC' || old('uf')==='SC' ? 'selected="selected"' : '') }}>SC</option>
                                    <option {{ (isset($data->uf) && $data->uf==='SP' || old('uf')==='SP' ? 'selected="selected"' : '') }}>SP</option>
                                    <option {{ (isset($data->uf) && $data->uf==='SE' || old('uf')==='SE' ? 'selected="selected"' : '') }}>SE</option>
                                    <option {{ (isset($data->uf) && $data->uf==='TO' || old('uf')==='TO' ? 'selected="selected"' : '') }}>TO</option>  
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-3">
                            <div class="form-group">
                                <div class="switch-box">
                                    <label for="status">Ativo</label>

                                    <label class="switch">
                                        <input name="status" type="checkbox" {{(isset($data->status) && (int)$data->status === 1 || old('status') === '1' ? 'checked="checked"' : '')}} value="1" />
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr />

                    <div id="api-messages"></div>

                    <div class="row my-5">
                        <div class="col-12">
                            <button class="btn btn-warning d-block" style="width:100%">Atualizar</button>
                        </div>
                    </div>

                </form>
                
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    
    <script>

        function postRequest(event) {
            event.preventDefault();

            document.getElementById('api-messages').classList.remove('alert','alert-info','text-center');
            document.getElementById('api-messages').innerHTML = '';

            const options = {
            url: "{{ route('api.user.update',[$data->id]) }}",
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json;charset=UTF-8'
            },
            data: new FormData(document.getElementById('form'))
            };

            axios({
                method: options.method,
                url: options.url,
                data: options.data,
                headers: options.headers,
            })
            .then(function (response) {
                
                let status = response.data.status;
                let cod = response.data.cod;
                let messages = '';

                if(cod === 200){
                    
                    messages += '<li>Cadastro atualizado com sucesso</li>';
                
                } else {

                    status.forEach(function(msg){
                        messages += `<li>${msg}</li>`;
                    });

                }

                document.getElementById('api-messages').classList.add('alert','alert-info','text-center');
                document.getElementById('api-messages').innerHTML = `<ul>${messages}</ul>`;

            })
            .catch(function (response) {
                //handle error
                console.log('Erro: ', response);
            });
            
        }

        const form = document.getElementById('form');
        form.addEventListener('submit', postRequest);

    </script>

@endsection