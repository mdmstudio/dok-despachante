@extends('layouts.app')

@section('content')

    <div class="mt-8 dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        <div class="grid grid-cols-1 md:grid-cols-1">
            
            <div class="p-6 border-t border-gray-200 dark:border-gray-700">
                <div class="flex items-center relative">
                    <img src="{{asset('svg/chat.svg')}}" alt="Usuários" width="30" height="auto">
                    <div class="ml-4 leading-7 dark:text-white">
                        <h1>{{(isset($data) && $data->count()) ? $data->total() . ' ' : ''}}Usuários</h1>
                    </div>

                    <div>
                        <a href="{{ route('user.create') }}" class="btn btn-warning btn-new">Novo Usuário</a>
                    </div>

                </div>
                
                <div class="text-gray-600 dark:text-gray-400 text-sm">
                    Gerencie os usuários cadastrados na plataforma.
                </div>
  

                @if (isset($data) && $data->count())

                <table class="table my-5 dark:text-white" style="font-size:0.7em">

                    <tr>
                        <th>&nbsp;</th>
                        <th>Nome</th>
                        <th>Tipo</th>
                        <th>Localidade</th>
                        <th>Status</th>
                        <th>Data</th>
                        <th>&nbsp;</th>
                    </tr>

                    @foreach ($data as $d)
                    <tr>
                        <td><a href="{{ route('user.show',[$d->id]) }}" class="btn btn-warning badge">Ver</a></td>
                        <td>
                            {{ $d->name }}<br />
                            <a href="mailto:{{ $d->email }}" title="Enviar mensagem">{{ $d->email }}</a>
                        </td>
                        <td><span class="badge badge-{{ strtolower($d->type) }}">{{ $d->type() }}</span></td>
                        <td>{{ $d->uf }}</td>
                        <td><span class="badge badge-{{ ($d->status) ? 'active' : 'inactive' }}">{{ $d->status() }}</span></td>
                        <td>{{ $d->created_at() }}</td>
                        <td class="actions-icons">

                            @if (\Auth::user())

                            <a href="{{ route('user.edit',[$d->id]) }}" class="mx-2"><img src="{{asset('svg/edit.svg')}}" alt="Atualizar registro" width="15" height="auto" /></a>
                            
                            <form action="{{ route('user.destroy',[$d->id]) }}" method="POST" enctype="application/x-www-form-urlencoded" class="d-inline-block">
                                @method('DELETE')
                                @csrf
                                <button type="submit"><img src="{{asset('svg/trash.svg')}}" alt="Remover registro" width="15" height="auto" /></button>
                            </form>
                                                            
                            @endif

                        </td>
                    </tr>
                    @endforeach

                </table>

                {{ $data->links('layouts.pagination') }}

                @else

                <div class="alert alert-info mt-4 text-center">Não há usuários cadastrados</div>

                @endif

                
            </div>

        </div>
    </div>

@endsection