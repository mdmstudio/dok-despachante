@extends('layouts.app')

@section('content')

    <div class="mt-8 dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        <div class="grid grid-cols-1 md:grid-cols-1">
            
            <div class="p-6 border-t border-gray-200 dark:border-gray-700">
                <div class="flex items-center relative">
                    <img src="{{asset('svg/chat.svg')}}" alt="Usuários" width="30" height="auto">
                    <div class="ml-4 leading-7 dark:text-white">
                        <h1>Login</h1>
                    </div>
                    <div>
                        <a href="{{ route('user.index') }}" class="btn btn-dark btn-new">Voltar</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 dark:text-white">
                        Faça o login para poder atualizar e remover usuários
                    </div>
                </div>

                <form method="POST" action="{{ route('login') }}" class="my-5">
                    @csrf

                    <div class="row mb-3">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="title">E-mail</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="title">Senha</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                        </div>
                    </div>

                    <hr />

                    <div class="row my-5 text-center">
                        <div class="col-12">
                            <button type="submit" class="btn btn-warning">
                                {{ __('Login') }}
                            </button>
                        </div>
                    </div>

                </form>
                
            </div>

        </div>
    </div>

@endsection