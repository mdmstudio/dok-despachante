<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Str;

use App\Models\User;

class UserTest extends TestCase
{
    /**
     * Users have methods that allows:
     * Get Data (all)
     * Get Data (specific)
     * Store Data
     * Update Data
     * Destroy Data
     */

    public function test_user_was_created()
    {

        $user = new User;

        $create = [
            'name' => 'Usuário cadastrado',
            'description' => 'Teste',
            'email' => 'user@test.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token' => Str::random(10),
            'type' => 'PF',
            'uf' => 'SP',
            'status' => 1,
        ];

        $created = $user->create($create);
        $this->assertObjectHasAttribute('attributes',$created);
    }

    public function test_user_was_updated()
    {
        $user = User::first();

        $update = [
            'title' => 'Usuário Atualizado',
            'description' => $user->description,
            'type' => $user->type,
            'uf' => $user->uf,
            'status' => $user->status,
        ];

        $updated = $user->update($update);
        $this->assertTrue($updated);
    }

    public function test_user_was_deleted()
    {
        $user = User::first();
        $user->delete();
        $this->assertDeleted($user);
    }

    public function test_a_visitor_can_see_users()
    {
        $response = $this->get('/users');
        $response->assertStatus(200);
    }

    public function test_a_visitor_can_see_specific_user()
    {
        $user = User::first();
        $response = $this->get('/users/show/'.$user->id);
        $response->assertStatus(200);
    }

}
