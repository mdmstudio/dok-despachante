<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use \Validator;


class UserController extends Controller
{

    use RegistersUsers;

    protected $view_prefix = "users.";
    protected $route_index = 'user.index';
    protected $redirectTo = RouteServiceProvider::HOME;

    public function index()
    {
        
        $data = User::orderByDesc('id')->paginate(15);
        return view($this->view_prefix.'index', compact('data') );

    }

    public function create()
    {
        return view($this->view_prefix.'create');
    }

    public function store(Request $request)
    {


        $request->merge(['status'=>(isset($request->status) ? 1 : 0)]);
        
        $messages = [
            'name.required' => 'Por favor informe o título do usuário',
            'name.max' => 'O nome do usuário deve possuir no máximo :max caracteres',
            'type.required' => 'Por favor informe o tipo do usuário',
            'type.in' => 'O tipo do usuário informado não é aceito pelo sistema',
            'uf.required' => 'Por favor informe a localidade do usuário',
            'uf.in' => 'A localidade informada do usuário não é aceita pelo sistema',
            'status.required' => 'Por favor informe o status do usuário',
            'status.boolean' => 'O status informado para o usuário não parece ser válido',
            'email.required' => 'Por favor informe um e-mail/credencial para o usuário',
            'email.max' => 'O e-mail do usuário deve possuir no máximo :max caracteres',
            'email.unique' => 'O e-mail informado já está sendo utilizado por outro usuário',
            'password.required' => 'A senha é obrigatória',
            'password.min' => 'A senha de acesso deve ter no mínimo :min caracteres',
            'password.confirmed' => 'Parece que as senhas não conferem'
        ];
        
        $validator = Validator::make($request->all(), [
            'name' => ['required','max:155'],
            'type' => ['required','in:PF,PJ,BOTH'],
            'uf' => ['required','in:AC,AL,AP,AM,BA,CE,DF,ES,GO,MA,MT,MS,MG,PA,PB,PR,PE,PI,RJ,RN,RS,RO,RR,SC,SP,SE,TO'],
            'status' => ['required','boolean'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ], $messages);

        if($validator->fails()){

            if (\Request::wantsJson()) {

                $messages = [];
                foreach($validator->errors()->getMessages() as $key => $message):
                    $messages[] = implode(",",$message);
                endforeach;

                return response()->json([
                    'status' => $messages,
                    'cod' => 400
                ]);
            }

            return redirect()->back()->withErrors($validator->messages())->withInput();
        }

        $request->merge(['password' => Hash::make($request->password)]);

        User::create($request->all());

        if (\Request::wantsJson()) {
            return response()->json([
                'status' => 'OK',
                'cod' => 200
            ]);
        }

        return redirect()->route($this->route_index);
    }

    public function show($id)
    {
        $data = User::findOrFail($id);
        return view($this->view_prefix.'show', compact(['data']));
    }

    public function edit($id)
    {
        $data = User::findOrFail($id);
        return view($this->view_prefix.'edit', compact(['data']));
    }

    public function update(Request $request, $id)
    {

        $data = User::findOrFail($id);
        $password = $data->password;

        $request->merge(['status'=>(isset($request->status) ? 1 : 0)]);
        
        $messages = [
            'name.required' => 'Por favor informe o título do usuário',
            'name.max' => 'O nome do usuário deve possuir no máximo :max caracteres',
            'type.required' => 'Por favor informe o tipo do usuário',
            'type.in' => 'O tipo do usuário informado não é aceito pelo sistema',
            'uf.required' => 'Por favor informe a localidade do usuário',
            'uf.in' => 'A localidade informada do usuário não é aceita pelo sistema',
            'status.required' => 'Por favor informe o status do usuário',
            'status.boolean' => 'O status informado para o usuário não parece ser válido',
            'email.required' => 'Por favor informe um e-mail/credencial para o usuário',
            'email.max' => 'O e-mail do usuário deve possuir no máximo :max caracteres'
        ];

        $fields = [
            'name' => ['required','max:155'],
            'type' => ['required','in:PF,PJ,BOTH'],
            'uf' => ['required','in:AC,AL,AP,AM,BA,CE,DF,ES,GO,MA,MT,MS,MG,PA,PB,PR,PE,PI,RJ,RN,RS,RO,RR,SC,SP,SE,TO'],
            'status' => ['required','boolean']
        ];
        
        if(isset($request->email) && $request->email !== $data->email){
            $messages += [
                'email.unique' => 'O e-mail informado já está sendo utilizado por outro usuário'
            ];

            $fields += [
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users']
            ];
        }

        if(isset($request->password) && $request->password){
            $messages += [
                'password.required' => 'A senha é obrigatória',
                'password.min' => 'A senha de acesso deve ter no mínimo :min caracteres',
                'password.confirmed' => 'Parece que as senhas não conferem'
            ];

            $fields += [
                'password' => ['required', 'string', 'min:8', 'confirmed']
            ];

            $password = Hash::make($request->password);

        }
        
        $validator = Validator::make($request->all(), $fields, $messages);

        if($validator->fails()){

            if (\Request::wantsJson()) {

                $messages = [];
                foreach($validator->errors()->getMessages() as $key => $message):
                    $messages[] = implode(",",$message);
                endforeach;

                return response()->json([
                    'status' => $messages,
                    'cod' => 400
                ]);
            }

            return redirect()->back()->withErrors($validator->messages())->withInput();
        }

        $request->merge(['password' => $password]);

        $data->update($request->all());

        if (\Request::wantsJson()) {
            return response()->json([
                'status' => 'OK',
                'cod' => 200
            ]);
        }

        return redirect()->route($this->route_index);
    }

    public function destroy($id)
    {
        $data = User::findOrFail($id);
        $data->delete();
        return redirect()->route($this->route_index);
    }

}
