<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'email',
        'password',
        'type',
        'uf',
        'status'        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function created_at()
    {
        return date('d/m/Y H:i:s', strtotime($this->created_at));
    }

    public function type()
    {
        switch(strtoupper($this->type))
        {
            case 'PF': return 'Pessoa Física'; break;
            case 'PJ': return 'Pessoa Jurídica'; break;
            case 'BOTH': return 'Pessoa Física e Jurídica'; break;
            default: return 'Tipo não encontrado: ' . $this->type;
        }
    }

    public function status()
    {
        return ($this->status) ? 'Ativo' : 'Inativo';
    }

    public function status_badge()
    {
        return ($this->status) ? 'active' : 'inactive';
    }
}
