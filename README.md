![alt text](https://gitlab.com/mdmstudio/dok-despachante/-/raw/f42236f5eef3c0bc36fe1762042a105c24bf9875/public/img/preview-readme.png?raw=true)

## Como instalar e testar o Gestor de Usuários DOK Despachante

Você precisa ter o Laravel (e o composer) Instalado em seu computador. Para isto, basta rodar o comando:

- $ composer global require laravel/installer

Também precisa baixar o MAMP para conseguir rodar o PHPMyAdmin / MySQL

- https://www.mamp.info/en/downloads/

E, então, clonar este repositório em seu computador

- https://gitlab.com/mdmstudio/dok-despachante.git

## Instalando

Configure o arquivo .env na raiz do projeto Laravel com as credenciais de seu banco de dados MySQL ou MariaDB

- Configurar o arquivo .env

Acesse a pasta onde você descompactou o repositório e execute o comando:

- composer install
- composer update
- php artisan migrate:fresh --seed

O instalador fará algumas perguntas para rodar:

- Seeders
- PHPUnit Tests
- Caso queira rodar os testes: php artisan test

## Melhorias

Possíveis melhorias futuras:

- Uso de Docker
- Vue.js para controlar o CRUD de maneira reativa
- Melhores testes
- Documentação via Swagger
- Melhoria do Layout/UX
- etc.

Considero que quem irá rodar esse projeto para efeitos de validação possui conhecimento de Laravel/MAMP, por esta razão a explicação foi sucinta.
